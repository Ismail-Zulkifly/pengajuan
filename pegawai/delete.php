<?php

$connect = mysqli_connect("localhost", "root", "", "pengajuan");
if (isset($_POST["employee_id"])) {
    $output = '';
    $query = "
    DELETE from pegawai where nip = '" . $_POST["employee_id"] . "'
    ";
    if (mysqli_query($connect, $query)) {
        $output .= '<label class="text-success">Data Berhasil Dihapus</label>';
        $select_query = "SELECT * FROM pegawai ORDER BY nip ASC";
        $result = mysqli_query($connect, $select_query);
        $output .= '
      <table class="table table-bordered">  
                    <tr>  
                    <th>No</th>
                         <th width="55%">Nama Pegawai</th>  
                         <th width="15%">Lihat</th>  
                         <th width="15%">Edit</th>  
                         <th width="15%">Hapus</th>  
                    </tr>
     ';
        $no = 1;
        while ($row = mysqli_fetch_array($result)) {
            $output .= '
       <tr>  
            <td>' . $no . '</td> 
             <td>' . $row["nama"] . '</td>  
            <td><input type="button" name="view" value="Lihat Detail" id="' . $row["nip"] . '" class="btn btn-info btn-xs view_data" /></td>
			<td><input type="button" name="edit" value="Edit" id="' . $row["nip"] . '" class="btn btn-warning btn-xs edit_data" /></td> 									 
            <td><input type="button" name="delete" value="Hapus" id="' . $row["nip"] . '" class="btn btn-danger btn-xs hapus_data" /></td>
					 
                    </tr>
      ';
            $no++;
        }
        $output .= '</table>';
    } else {
        $output .= mysqli_error($connect);
    }
    echo $output;
}
