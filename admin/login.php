<?php
$connect = mysqli_connect("localhost", "root", "", "pengajuan");
$query = "SELECT * FROM user WHERE user = ' ' ";
$result = mysqli_query($connect, $query);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="modal animate__animated animate__bounceIn  modal-dialog-centered" id="modalLogin" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm  shadow">
            <div class=" modal-content">
                <div class="modal-header p-4">
                    <h5 class="modal-title" id="staticBackdropLabel">Silahkan Login</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-sm-12  col-xs-12">
                            <div class="input-group mb-4 input-group-md">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" class="form-control" name="txtUser" id="nama" placeholder="Nama" required autocomplete="off" />
                            </div>
                        </div>
                        <div class="col-sm-12  col-xs-12">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" class="form-control " name="txtPass" id="pass" placeholder="Kata Sandi" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p style="color:#777474;">&copy; <?php echo date("Y") ?> BPS Gorontalo</p>
                    <button type="submit" id="btnLogin" onClick="cekLogin()" class="btn btn-info">Login</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>